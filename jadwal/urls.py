from django.urls import path
from . import views
app_name = "jadwal"

urlpatterns = [
    path('', views.add, name = "tambah_jadwal"),
    path('delete/<str:pk>/',views.delete, name='delete'),
]