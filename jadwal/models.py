from django.db import models

class Jadwal(models.Model):
    matkul = models.CharField(max_length=120)
    dosen = models.CharField(max_length=120)
    sks = models.CharField(max_length=120)
    deskripsi = models.CharField(max_length=120)
    semester = models.CharField(max_length=120)
    ruang = models.CharField(max_length=120)

# Create your models here.
