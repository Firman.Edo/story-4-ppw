from django.shortcuts import render, redirect
from .models import Jadwal
# Create your views here.

def add(request):
    Parameter = False
    nama = ""

    bebas = Jadwal.objects.all()

    if request.method == "POST":
        Parameter = True
        matkul1 = request.POST.get("nama")
        dosen1 = request.POST.get("dosen")
        sks1 = request.POST.get("sks")
        deskripsi1 = request.POST.get("deskripsi")
        semester1 = request.POST.get("semester")
        ruang1 = request.POST.get("ruang")
        p = Jadwal.objects.create(matkul = matkul1, dosen = dosen1, sks = sks1, deskripsi = deskripsi1, semester = semester1, ruang = ruang1)
        p.save()
        nama = matkul1

    context = {
        'param' : Parameter,
        'bebas1' : bebas
    }
    return render(request, 'overall.html', context)

def delete(request, pk):
    item=Jadwal.objects.get(id=pk)
    if request.method == "POST":
        item.delete()
        return redirect("/jadwal")
    context ={
        'matkuli': item,
    }
    return render(request,'hapus.html',context)