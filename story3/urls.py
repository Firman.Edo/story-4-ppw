from django.urls import include, path
from .views import index_1, index_2

urlpatterns = [
    path('', index_1, name='index4'),
    path('bio/', index_2, name='index3'),
]