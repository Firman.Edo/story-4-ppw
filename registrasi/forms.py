from django import forms


class jadwalku(forms.Form):
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Name',
        'required': True,
    }))

    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Mata Kuliah',
        'required': True,
    }))

    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Dosen Pengajar Matkul',
        'required': True,
    }))
    Jamnya = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jam Mata Kuliah',
        'required': True,
    }))
    sks = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'SKS Mata Kuliah',
        'required': True,
    }))
    Semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Semester berapa',
        'required': True,
    }))
    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi Mata Kuliah',
        'required': True,
    }))

