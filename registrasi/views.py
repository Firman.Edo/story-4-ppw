from django.shortcuts import render, redirect
from .models import kelas as schedule
from .forms import jadwalku
from . import forms, models
# Create your views here.

def contact(request):
    form = forms.jadwalku(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            sched = schedule()
            sched_nama = form.cleaned_data['nama']
            sched_mata_kuliah = form.cleaned_data['mata_kuliah']  
            schde_dosen = form.cleaned_data['dosen']
            sched_Jamnya = form.cleaned_data['Jamnya']
            sched_sks = form.cleaned_data['sks']
            sched_Semester = form.cleaned_data['Semester']
            sched_deskripsi = form.cleaned_data['deskripsi']
            sched.save()
            return redirect ("/schedule")
   
    sched = schedule.objects.all()
    form = jadwalku()
    response = {"sched":sched, 'form':form}
    return render (request, 'over.html', response) 

def delete(request, pk):
    form = forms.jadwalku(request.POST)
    if request.method == "POST" :
        if form.is_valid():
            sched = schedule()  
            sched_nama = form.cleaned_data['nama']
            sched_mata_kuliah = form.cleaned_data['mata_kuliah']
            sched_dosen = form.cleaned_data['dosen_pengajar']
            sched_Jamnya = form.cleaned_data['Jamnya']
            sched_sks = form.cleaned_data['jumlah_sks']
            sched_Semester = form.cleaned_data['semester']
            sched_deskripsi = form.cleaned_data['deskripsi']
            sched.save()
            return redirect ("/schedule")
    
    schedule.objects.filter(pk = pk).delete()
    form = jadwalku()
    matkul_data = models.schedlue.objects.all()
    response = {"sched":matkul_data, 'form':form}
    return redirect('/schedule')

def detail(request, pk):
    matkul = models.schedlue.objects.get(pk=pk)
    context_detail= {
        "matkul" : matkul,

    }
    return render(request, 'register.html', context_detail)