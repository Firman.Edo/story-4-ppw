from django.urls import path
from . import views
from .views import contact, delete

app_name = 'registrasi'

urlpatterns = [
    path('', contact, name = "over"),
    path('detail/<int:pk>',views.detail, name="detail"),
    path('<int:pk>', delete, name="Delete"),

]
