from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from . import models
from .views import index

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response=Client().get('/kegiatansaya/')
        self.assertEqual(response.status_code,200)

    def test_story6_model_create(self):
        models.Kegiatan.objects.create(nama='Demo TK1')
        jumlah = models.Kegiatan.objects.all().count()
        self.assertEqual(jumlah,1)

    def test_story6_model_create2(self):
        models.Kegiatan.objects.create(nama='Demo TK1')
        kegiatan = models.Kegiatan.objects.get(nama='Demo TK1')
        models.PesertaKegiatan.objects.create(nama='Andrew', kegiatan=kegiatan)
        jumlah = models.PesertaKegiatan.objects.all().count()
        self.assertEqual(jumlah,1)
    
    def test_story6_model_relational(self):
        models.Kegiatan.objects.create(nama='Demo TK1')
        idpk = models.Kegiatan.objects.all()[0].id
        kegiatan = models.Kegiatan.objects.get(id=idpk)
        models.PesertaKegiatan.objects.create(nama='Andrew', kegiatan=kegiatan)
        jumlah = models.PesertaKegiatan.objects.filter(kegiatan=kegiatan).count()
        self.assertEqual(jumlah,1)
    
    def test_story6_template_used(self):
        response = Client().get('/kegiatansaya/')
        self.assertTemplateUsed(response, 'kegiatan.html')
    
    def test_story6_view_show(self):
        models.Kegiatan.objects.create(nama='Test')
        request = HttpRequest()
        response = index(request)
        html_respone = response.content.decode('utf8')
        self.assertIn('Test', html_respone)

    def test_story6_view_show_peserta(self):
        models.Kegiatan.objects.create(nama='Test')
        idpk = models.Kegiatan.objects.all()[0].id
        kegiatan = models.Kegiatan.objects.get(id=idpk)
        models.PesertaKegiatan.objects.create(nama='AndrewTest', kegiatan=kegiatan)
        request = HttpRequest()
        response = index(request)
        html_respone = response.content.decode('utf8')
        self.assertIn('AndrewTest', html_respone)

    def test_story6_using_index(self):
        found = resolve('/kegiatansaya/')
        self.assertEqual(found.func, index)
    
    def test_story6_save_a_POST_request(self):
        response = Client().post('/kegiatansaya/', data={'nama_kegiatan':'Demo Story 6'})
        jumlah = models.Kegiatan.objects.filter(nama='Demo Story 6').count()
        self.assertEqual(jumlah,1)
    
    def test_story6_bad_request(self):
        response = Client().post('/kegiatansaya/', data={'nama_peserta':'ABC', 'id_kegiatan':99})
        self.assertEqual(response.status_code, 400)

    def test_story6_save_a_POST_request_2(self):
        models.Kegiatan.objects.create(nama='Test')
        response = Client().post('/kegiatansaya/', data={'nama_peserta':'ABC', 'id_kegiatan':1})
        self.assertEqual(response.status_code,200)

    def test_story6_delete_POST_request(self):
        models.Kegiatan.objects.create(nama='Test')
        idpk = models.Kegiatan.objects.all()[0].id
        kegiatan = models.Kegiatan.objects.get(id=idpk)
        models.PesertaKegiatan.objects.create(nama='AndrewTest', kegiatan=kegiatan)
        response = Client().post('/kegiatansaya/', data={'delete-peserta':1})
        self.assertEqual(response.status_code,200)

    def test_story6_delete_POST_bad_request(self):
        response = Client().post('/kegiatansaya/', data={'delete-peserta':99})
        self.assertEqual(response.status_code,400)

    def test_story6_model_name(self):
        models.Kegiatan.objects.create(nama='Test')
        kegiatan = models.Kegiatan.objects.get(nama='Test')
        self.assertEqual(str(kegiatan),'Test')

    def test_story6_model_name2(self):
        models.Kegiatan.objects.create(nama='Test')
        idpk = models.Kegiatan.objects.all()[0].id
        kegiatan = models.Kegiatan.objects.get(id=idpk)
        models.PesertaKegiatan.objects.create(nama='AndrewTest', kegiatan=kegiatan)
        peserta = models.PesertaKegiatan.objects.get(kegiatan=kegiatan)
        self.assertEqual(str(peserta),'AndrewTest')
